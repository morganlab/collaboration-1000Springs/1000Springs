for i in *.gz; do
	echo starting ${i}
	kraken2 --db /DATABASES/KRAKEN/CustomKraken --use-names --threads 8 --gzip-compressed --output - --report ${i}_report.txt $i
	echo ${i} finished
done
