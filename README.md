#  **27 April 2023**

This repository contains code used for analysis presented in the first three manuscripts from the [1000 Springs Project](http://1000Springs.org.nz), a catalogue of microbial biodiversity and geochemical information for 1,000 geothermal springs in New Zealand’s central North Island.

***

#### Manuscript 1
Power, J. F. et al. (2018) Microbial biogeography of 925 geothermal springs in New Zealand. *Nat. Commun.* 9:2876

Online version: https://doi.org/10.1038/s41467-018-05020-y | PDF version: https://rdcu.be/3KDJ

#### Authors
Jean F. Power, Carlo R. Carere, Charles K. Lee, Georgia L.J. Wakerley, David W. Evans, Mathew Button, Duncan White, Melissa D. Climo, Annika M. Hinze, Xochitl C. Morgan, Ian R. McDonald, S. Craig Cary & Matthew B. Stott

#### Corresponding authors
Dr. Matthew Stott: matthew.stott@canterbury.ac.nz; +64 (0)3 369 2511 | Prof. Craig Cary: caryc@waikato.ac.nz; +64 (0)7 838 4593

#### Abstract
Geothermal springs are model ecosystems to investigate microbial biogeography as they represent discrete, relatively homogenous habitats, are distributed across multiple geographical scales, span broad geochemical gradients, and have reduced metazoan interactions. Here, we report the largest known consolidated study of geothermal ecosystems to determine factors that influence biogeographical patterns. We measured bacterial and archaeal community composition, 46 physicochemical parameters, and metadata from 925 geothermal springs across New Zealand (13.9 – 100.6°C and pH <1–9.7). We determined that diversity is primarily influenced by pH at temperatures <70°C; with temperature only having a significant effect at >70°C. Further, community dissimilarity increases with increasing geographic distance, with niche selection driving assembly at a localised scale. Surprisingly, two genera (*Venenivibrio* and *Acidithiobacillus*) dominated in both average relative abundance (11.2 and 11.1%) and prevalence (74.2 and 62.9% respectively) These findings provide an unprecedented insight into ecological behaviour in geothermal springs, and a foundation to improve the characterisation of microbial biogeographical processes.

***

#### Manuscript 2
Power, J. F. et al. (2023) Temporal dynamics of geothermal microbial communities in Aotearoa-New Zealand. *Front. Microbiol.* 14:1094311 

Online version: https://doi.org/10.3389/fmicb.2023.1094311

#### Authors
Jean F. Power, Caitlin L. Lowe, Carlo R. Carere, Ian R. McDonald, S. Craig Cary & Matthew B. Stott

#### Corresponding authors
Dr. Matthew Stott: matthew.stott@canterbury.ac.nz; +64 (0)3 369 2511 | Prof. Craig Cary: caryc@waikato.ac.nz; +64 (0)7 838 4593

#### Abstract
Microbial biogeography studies, in particular for geothermal-associated habitats, have focused on spatial patterns and/or individual sites, which have limited ability to describe the dynamics of ecosystem behaviour. Here, we report the first comprehensive temporal study of bacterial and archaeal communities from an extensive range of geothermal features in Aotearoa-New Zealand. One hundred and fifteen water column samples from 31 geothermal ecosystems were taken over a 34-month period to ascertain microbial community stability (control sites), community response to both natural and anthropogenic disturbances in the local environment (disturbed sites) and temporal variation in spring diversity across different pH values (pH 3, 5, 7, 9) all at a similar temperature of 60–70°C (pH sites). Identical methodologies were employed to measure microbial diversity via 16S rRNA gene amplicon sequencing, along with 44 physicochemical parameters from each feature, to ensure confidence in comparing samples across timeframes. Our results indicated temperature and associated groundwater physicochemistry were the most likely parameters to vary stochastically in these geothermal features, with community abundances rather than composition more readily affected by a changing environment. However, variation in pH (pH ±1) had a more significant effect on community structure than temperature (±20°C), with alpha diversity failing to adequately measure temporal microbial disparity in geothermal features outside of circumneutral conditions. While a substantial physicochemical disturbance was required to shift community structures at the phylum level, geothermal ecosystems were resilient at this broad taxonomic rank and returned to a pre-disturbed state if environmental conditions re-established. These findings highlight the diverse controls between different microbial communities within the same habitat-type, expanding our understanding of temporal dynamics in extreme ecosystems.

***

#### Manuscript 3
Power, J. F. et al. (2023) Allopatric speciation in the bacterial phylum Aquificota enables genus-level endemism in Aotearoa-New Zealand. *Unpublished*

#### Authors
Jean F. Power, Carlo R. Carere, Holly E. Welford, Daniel T. Hudson, Kevin C. Lee, John W. Moreau, Thijs J. G. Ettema, Anna-Louise Reysenbach, Charles K. Lee, Daniel R. Colman, Eric S. Boyd, Xochitl C. Morgan, Ian R. McDonald, S. Craig Cary & Matthew B. Stott

#### Corresponding authors
Dr. Matthew Stott: matthew.stott@canterbury.ac.nz; +64 (0)3 369 2511 | Prof. Craig Cary: caryc@waikato.ac.nz; +64 (0)7 838 4593

#### Abstract
Allopatric speciation has been difficult to examine among microorganisms, with prior reports of endemism restricted to sub-genus level taxa. Here, we present evidence of an endemic bacterial genus, Venenivibrio (phylum Aquificota), from the Taupō Volcanic Zone (TVZ), Aotearoa-New Zealand. 16S rRNA gene sequencing revealed widespread distribution of Venenivibrio in TVZ geothermal springs (74 %, n=686), with maximal read abundance occurring at pH 4-6, 50-70 °C, and low oxidation-reduction potentials. Genomic analysis and culture-based experiments of the only characterised species for the genus, Venenivibrio stagnispumantis CP.B2T, confirmed a chemolithoautotrophic metabolism dependent on hydrogen oxidation, further highlighting a specific environmental niche that could enhance habitat isolation. While similarity between Venenivibrio populations illustrated dispersal is not limited across the TVZ, extensive amplicon, metagenomic, and phylogenomic analyses of global microbial communities from DNA sequence databases indicates Venenivibrio is geographically restricted to the Aotearoa-New Zealand archipelago. We conclude that combined geographical and physicochemical constraints have resulted in the establishment of an endemic bacterial genus.

***

### Instructions for this repository
Manuscript 1: R code is available for Fig. 2-5 in the main text and Supplementary Fig. 1-9 in the Supplementary Information in the **code** directory. All figure code is accompanied by corresponding statistical analyses. Output figures directly from R are in the **output** directory. These have subsequently been tweaked in Inkscape and final versions used in the manuscript are in the **figures** directory.

Manuscript 2: R code is available for all categories of samples (A, B & C) under **temporal_manuscript**. PDFs of temporal figures are available in the **temporal_figures** directory.

Manuscript 3: R code for this manuscript is available in the **venenivibrio_manuscript** directory. All metagenome analysis can be found in **metagenome_alignments**. PDFs of main figures are available in the **venenivibrio_figures** directory. Code for Aquificota analysis can be found in **venenivibrio_code**, with all remaining code currently being uploaded (as of 27 April 2023).

Please visit http://1000Springs.org.nz or https://www.facebook.com/1000SpringsProject/ for more information.
